import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import 'home_bloc_loaded_screen.dart';
import '../extra/loading.dart';
import '../extra/error_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  const HomeBlocScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (context, state) {
      if (state is HomeBlocLoadedState) {
        return HomeBlocLoadedScreen(data: state.data);
      } else if (state is HomeBlocLoadingState) {
        return LoadingIndicator();
      } else if (state is HomeBlocInitialState) {
        return Scaffold();
      } else if (state is HomeBlocErrorState) {
        return ErrorScreen(
          message: state.error != 'no-internet' ? state.error : '',
          icon: state.error != 'no-internet'
              ? null
              : Icon(
                  Icons.signal_wifi_connected_no_internet_4,
                  size: 100,
                  color: Colors.red,
                ),
          retryButton: CustomButton(
            text: 'Retry',
            height: 100,
            width: 100,
            leadingIcon: SizedBox(),
          ),
          retry: () {
            print('cek');
            final homeCubit = context.read<HomeBlocCubit>();
            homeCubit.fetchingData();
          },
        );
      }

      return Center(
          child: Text(kDebugMode ? "state not implemented $state" : ""));
    });
  }
}
