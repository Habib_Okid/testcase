import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/extra/no_data.dart';

class MoviewDetailScreen extends StatelessWidget {
  final Data? data;

  const MoviewDetailScreen({Key? key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('data: ${data!.series}');
    return Scaffold(
      appBar: AppBar(
        title: Text(
          data!.l!,
        ),
      ),
      body: detailWidget(data!),
    );
  }

  Widget detailWidget(Data data) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
              child: Image.network(data.i!.imageUrl!),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Text(
                data.l! + ' (${data.year!.toString()})',
                textDirection: TextDirection.ltr,
                style: TextStyle(
                  color: Colors.black87,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Divider(
              thickness: 1,
            ),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                child: Text(
                  'Other series : ',
                  textDirection: TextDirection.ltr,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
            data.series == null
                ? NoDataText()
                : data.series!.isEmpty
                    ? NoDataText()
                    : SizedBox(
                        height: 200,
                        child: ListView.builder(
                          itemCount: data.series!.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {},
                              child: movieItemWidget(data.series![index]),
                            );
                          },
                        ),
                      ),
          ],
        ),
      ),
    );
  }

  Widget movieItemWidget(Series data) {
    return Card(
      child: SizedBox(
        height: 170,
        width: 170,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
              child: Image.network(
                data.i!.imageUrl!,
                fit: BoxFit.fitHeight,
                height: 120,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Text(
                data.l!,
                textDirection: TextDirection.ltr,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                style: TextStyle(
                  color: Colors.black87,
                  fontSize: 15,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
