import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;
import 'package:majootestcase/utils/constant.dart';

class ApiServices {
  Future<MovieResponse?> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      Response<String> response =
          await dio.get(dio.options.baseUrl = Api.MOVIE_LIST);
      MovieResponse movieResponse =
          MovieResponse.fromJson(jsonDecode(response.data!));
      return movieResponse;
    } on DioError catch (e) {
      print(e.toString());
      return null;
    }
  }
}
