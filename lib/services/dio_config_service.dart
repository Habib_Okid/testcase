import 'dart:async';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio? dioInstance;
createInstance() async {
  var options = BaseOptions(
      baseUrl: "https://imdb8.p.rapidapi.com/auto-complete?q=iron",
      connectTimeout: 12000,
      receiveTimeout: 12000,
      headers: {
        "x-rapidapi-key": "0f353955bamsh8cf02ad109596c2p1527dejsnc446ca6e4a07",
        "x-rapidapi-host": "imdb8.p.rapidapi.com"
      });
  dioInstance = new Dio(options);
  dioInstance!.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));
}

Future<Dio> dio() async {
  await createInstance();
  dioInstance!.options.baseUrl =
      "https://imdb8.p.rapidapi.com/auto-complete?q=iron";
  return dioInstance!;
}
