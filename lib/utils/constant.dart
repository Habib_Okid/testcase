class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
  static const BASE_MOVIE = "https://imdb8.p.rapidapi.com/";
  static const MOVIE_LIST = "${BASE_MOVIE}auto-complete?q=game%20of%20thr";
}

class Font {}

String appTitle = "Majoo Test Case";

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
