import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/db_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    DatabaseHandler handler = DatabaseHandler();
    handler.login(user.email!).then((value) {
      emit(AuthBlocLoadingState());
      if (value != null) {
        if (value.password == user.password) {
          Future.delayed(const Duration(seconds: 2), () async {
            emit(AuthBlocSuccesState("Login Berhasil"));
            await sharedPreferences.setBool("is_logged_in", true);
            await sharedPreferences.setString("user_value", jsonEncode(value));
            emit(AuthBlocLoggedInState());
          });
        } else {
          Future.delayed(const Duration(seconds: 2), () {
            emit(AuthBlocErrorState("Password Salah"));
          });
        }
      } else {
        Future.delayed(const Duration(seconds: 2), () {
          emit(AuthBlocErrorState("Email Tidak Ditemukan"));
        });
      }
    });
  }

  void registerUser(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    DatabaseHandler handler = DatabaseHandler();
    handler.daftar(user.userName!, user.email!, user.password!).then((value) {
      emit(AuthBlocLoadingState());
      if (value != null) {
        Future.delayed(const Duration(milliseconds: 5500), () async {
          emit(AuthBlocSuccesState("Pendaftaran Berhasil"));
          await sharedPreferences.setBool("is_logged_in", true);
          await sharedPreferences.setString("user_value", jsonEncode(user));
          emit(AuthBlocLoggedInState());
        });
      } else {
        Future.delayed(const Duration(milliseconds: 5500), () {
          emit(AuthBlocErrorState("Pendaftaran Gagal"));
        });
      }
    });
  }
}
