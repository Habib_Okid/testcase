import 'package:majootestcase/models/user.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHandler {
  Future<Database> initializeDB() async {
    String path = await getDatabasesPath();
    return openDatabase(
      join(path, 'users.db'),
      onCreate: (database, version) async {
        await database.execute(
          "CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,email TEXT,password TEXT,username TEXT,createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)",
        );
      },
      version: 1,
    );
  }

  // register user
  Future<int?> daftar(
    String? username,
    String? email,
    String? password,
  ) async {
    final Database db = await initializeDB();
    final data = {
      "username": username,
      "email": email,
      "password": password,
    };
    try {
      final id = await db.insert('users', data,
          conflictAlgorithm: ConflictAlgorithm.replace);
      return id;
    } catch (e) {
      return null;
    }
  }

  // Detail User
  Future<User?> login(String email) async {
    final Database db = await initializeDB();
    try {
      final List<Map<String, Object?>> queryResult = await db.query(
        'users',
        where: "email = ?",
        whereArgs: [email],
      );
      return queryResult.map((e) => User.fromJson(e)).first;
    } catch (e) {
      return null;
    }
  }
}
