import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _usernameController = TextController(initialValue: "");
  final _emailController = TextController(initialValue: "");
  final _passwordController = TextController(initialValue: "");
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;
  bool _isLoginForm = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Selamat Datang',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  // color: colorBlue,
                ),
              ),
              Text(
                _isLoginForm
                    ? 'Silahkan login terlebih dahulu'
                    : 'Silahkan daftar terlebih dahulu',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(
                height: 9,
              ),
              _form(),
              SizedBox(
                height: 50,
              ),
              CustomButton(
                text: _isLoginForm ? 'Login' : 'Daftar',
                onPressed: () {
                  handleLogin(context);
                },
                height: 100,
                leadingIcon: SizedBox(),
              ),
              SizedBox(
                height: 50,
              ),
              _register(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          _isLoginForm
              ? SizedBox()
              : CustomTextFormField(
                  context: context,
                  controller: _usernameController,
                  hint: 'username',
                  label: 'Username',
                  textInputAction: TextInputAction.next,
                  suffixIcon: SizedBox(),
                ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            textInputAction: TextInputAction.next,
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val)
                    ? null
                    : 'Masukkan e-mail yang valid';
            },
            suffixIcon: SizedBox(),
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            textInputAction: TextInputAction.done,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () {
          setState(() {
            _isLoginForm = !_isLoginForm;
          });
        },
        child: RichText(
          text: TextSpan(
              text: _isLoginForm ? 'Belum punya akun? ' : 'Sudah punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: _isLoginForm ? 'Daftar' : 'Login',
                ),
              ]),
        ),
      ),
    );
  }

  void handleLogin(BuildContext context) async {
    final _username = _usernameController.value;
    final _email = _emailController.value;
    final _password = _passwordController.value;
    if (formKey.currentState?.validate() == true) {
      User user = User(
        email: _email,
        userName: _username,
        password: _password,
      );
      print(user.email);
      final authCubit = context.read<AuthBlocCubit>();
      if (_isLoginForm) {
        authCubit.loginUser(user);
      } else {
        authCubit.registerUser(user);
      }
    }
  }
}
