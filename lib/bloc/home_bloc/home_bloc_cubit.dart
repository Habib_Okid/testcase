import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());
  void fetchingData() async {
    emit(HomeBlocInitialState());
    ApiServices apiServices = ApiServices();
    emit(HomeBlocLoadingState());
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        MovieResponse? movieResponse = await apiServices.getMovieList();
        if (movieResponse == null) {
          emit(HomeBlocErrorState("Terjadi Error"));
        } else {
          emit(HomeBlocLoadedState(movieResponse.data!));
        }
      }
    } on SocketException catch (_) {
      print('not connected');
      emit(HomeBlocErrorState("no-internet"));
    }
  }
}
