import 'package:flutter/material.dart';

class NoDataText extends StatelessWidget {
  const NoDataText(
      {Key? key,
      this.height = 200,
      this.widht = double.infinity,
      this.text = 'Tidak ada data'})
      : super(key: key);
  final double? height;
  final double? widht;
  final String? text;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: widht,
      child: Center(
        child: Text(
          text!,
          style: TextStyle(
            fontSize: 15,
            color: Colors.black54,
          ),
        ),
      ),
    );
  }
}
