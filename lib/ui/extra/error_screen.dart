import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final String? message;
  final Function()? retry;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final Widget retryButton;
  final Widget? icon;

  const ErrorScreen(
      {Key? key,
      this.gap = 10,
      required this.retryButton,
      this.message = "",
      this.fontSize = 14,
      this.retry,
      this.textColor,
      this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              message!,
              style: TextStyle(fontSize: 12, color: textColor ?? Colors.black),
            ),
            retry != null
                ? Column(
                    children: [
                      icon == null ? SizedBox() : icon!,
                      SizedBox(
                        height: 50,
                      ),
                      InkWell(
                        onTap: retry,
                        child: retryButton,
                      ),
                    ],
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
